a=00001.png
b=00002.png

percentage=50

width=400
heigth=400
px=0
py=300
output="output.png"

region=${width}x${heigth}+${px}+${py}

if [ "$#" -ne 8 ]; then
    echo "Illegal number of parameters"
    echo "subim.sh first-image second-image percentage width heigth px py output-file"
    exit -1

    echo "Assumed as follows:"
    echo "subim.sh $a $b $percentage $width $heigth $px $py $output"
else
    a=$1
    b=$2
    percentage=$3
    
    width=$4
    heigth=$5
    px=$6
    py=$7
    output=$8
fi

region=${width}x${heigth}+${px}+${py}

echo "$region"
convert -extract $region $a output/tmp1.png
convert -extract $region $b output/tmp2.png
convert output/tmp1.png output/tmp2.png -compose dissolve -define compose:args=${percentage} -composite output/tmp3.png
# convert largeimage smallimage -geometry +X+Y -compose copy -composite resultimage
convert $a output/tmp3.png -geometry +${px}+${py} -compose copy -composite output/${output}
exit
# convert $a output/tmp3.png -compose atop -geometry=$region output/tmp4.png

