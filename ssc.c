/* File ssc.c
 * created/modified on Fr 19 Aug 2022 15:27:55 CEST

 * by creac for Fr 19 Aug 2022 15:27:55 CEST

 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

int blend(size_t w, size_t h, char *o, char *a, char *b, int px, int py, int percentage);

static int perc=40;

//#define VERBOSE 1
#define VERBOSE 0

static char *tmpfiles[] = { "tmp1.png", "tmp2.png", "tmp3.png", "tmp4.png", "tmp5.png" };
// int dispx[]= { 0, 640, 0,   640 };
// int dispy[]= { 0, 0,   320, 320 };
int moveup(char *fname);

int main (int argc, char *argv[]) {
	int i;
	size_t w, h;
	char *output;
	char *inputs[5];
	char *input;
	int n = argc-4;
	int disp;
	static int dispx[4], dispy[4];

	if (argc < 6 || argc > 9) {
		fprintf(stderr, "Illegal number of arguments -- aborting.\n");
		fprintf(stderr, "Expected args: ssc width heigth output displacement input1 input2 [...].\n");
		fprintf(stderr, "Note: Up to five inputs are expected.\n");
		exit(-1);
	}
	w = atoi(argv[1]);
	h = atoi(argv[2]);
	output = argv[3];
	for (i=4; i<argc; i++) inputs[i-4] = argv[i];
	input = inputs[0];
	dispx[1] = dispx[3] = w;
	dispy[2] = dispy[3] = h;

	if (VERBOSE) {
		printf("n = %d, inputs are ", n);
		for (i=0; i<n; i++) printf("%s, ", inputs[i]);
		putchar('\n');
	}

	switch(n) {
	 case 2:
		blend(w, h, output,      inputs[0],   inputs[1], dispx[0], dispy[0], perc);
		break;
	 case 3:
		blend(w, h, tmpfiles[0], inputs[0],   inputs[1], dispx[0], dispy[0], perc);
		moveup(tmpfiles[0]);
		blend(w, h, output,      tmpfiles[0], inputs[2], dispx[1], dispy[1], perc);
		break;
	 case 4:
		blend(w, h, tmpfiles[0], inputs[0],   inputs[1], dispx[0], dispy[0], perc);
		moveup(tmpfiles[0]);
		blend(w, h, tmpfiles[1], tmpfiles[0], inputs[2], dispx[1], dispy[1], perc);
		moveup(tmpfiles[1]);
		blend(w, h, output,      tmpfiles[1], inputs[3], dispx[2], dispy[2], perc);
		break;
	 case 5:
		blend(w, h, tmpfiles[0], inputs[0],   inputs[1], dispx[0], dispy[0], perc);
		moveup(tmpfiles[0]);
		blend(w, h, tmpfiles[1], tmpfiles[0], inputs[2], dispx[1], dispy[1], perc);
		moveup(tmpfiles[1]);
		blend(w, h, tmpfiles[2], tmpfiles[1], inputs[3], dispx[2], dispy[2], perc);
		moveup(tmpfiles[2]);
		blend(w, h, output,      tmpfiles[2], inputs[4], dispx[3], dispy[3], perc);
		break;
	}
	/*
	blend(w, h, tmpfiles[0], inputs[0],   inputs[1], dispx[0], dispy[0], perc-10);
	blend(w, h, tmpfiles[1], tmpfiles[0], inputs[2], dispx[1], dispy[1], perc);
	blend(w, h, tmpfiles[2], tmpfiles[1], inputs[3], dispx[2], dispy[2], perc+10);
	blend(w, h, output,      tmpfiles[2], inputs[4], dispx[3], dispy[3], perc+20);
	*/
	// blend(w, h, output, inputs[0], inputs[1], 0, 0, perc);
}

char *resized(char *x) {
	return "output/resized.png";
}
char *tiled(char *x) {
	return "output/tiled.png";
}
char *dissolved(char *x) {
	return "output/dissolved.png";
}

int sysprint(char *s) {
	if (VERBOSE) puts(s);
	system(s);
}

int blend(size_t w, size_t h, char *o, char *a, char *b, int px, int py, int percentage) {
	char systring[256];

	sprintf(systring, "convert %s -resize %zux%zu %s", b, w, h, resized(b));
	sysprint(systring);

	sprintf(systring, "convert -extract %zux%zu+%d+%d %s %s", w, h, px, py, a, tiled(a));
	sysprint(systring);

	sprintf(systring,
	 "convert %s %s  -compose dissolve -define compose:args=%d -composite %s",
	 resized(b), tiled(a), percentage, dissolved("x"));
	sysprint(systring);

	sprintf(systring,
	 "convert %s %s -geometry +%d+%d -compose copy -composite output/%s",
	 a, dissolved("x"), px, py, o);
	sysprint(systring);
}
int moveup(char *fname) {
	char systring[256];
	sprintf(systring, "cp output/%s .", fname);
	sysprint(systring);
}
/* End of file ssc.c */
