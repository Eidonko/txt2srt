mkdir out

for i in `seq 1 164`; do
  ii=$(printf '%05d' $i)
  echo "Processing frame $ii"
  cp V1/$ii.png output/frame$ii.png
done

for i in `seq 165 328`; do
  ii=$(printf '%05d' $i)
  echo "Processing frame $ii"
  i2=$(printf '%05d' $((i-164)))
  ssc 640 360 frame$ii.png V1/$ii.png V2/$i2.png
done


for i in `seq 329 919` ; do
  ii=$(printf '%05d' $i)
  echo "Processing frame $ii"
  i2=$(printf '%05d' $((i-164)))
  i3=$(printf '%05d' $((i-328)))
  ssc 640 360 frame$ii.png V1/$ii.png V2/$i2.png V3/$i3.png
done


for i in `seq 920 1803` ; do
  ii=$(printf '%05d' $i)
  echo "Processing frame $ii"
  i2=$(printf '%05d' $((i-164)))
  i3=$(printf '%05d' $((i-328)))
  i4=$(printf '%05d' $((i-919)))
  ssc 640 360 frame$ii.png V1/$ii.png V2/$i2.png V3/$i3.png V4/$i4.png
done


for i in `seq 1804 2388` ; do
  ii=$(printf '%05d' $i)
  echo "Processing frame $ii"
  i2=$(printf '%05d' $((i-164)))
  i3=$(printf '%05d' $((i-328)))
  i4=$(printf '%05d' $((i-919)))
  i5=$(printf '%05d' $((i-1803)))
  ssc 640 360 frame$ii.png V5/$i5.png V2/$i2.png V3/$i3.png V4/$i4.png V1/$ii.png
done


for i in `seq 2389 2517` ; do
  ii=$(printf '%05d' $i)
  echo "Processing frame $ii"
  i2=$(printf '%05d' $((i-164)))
  i3=$(printf '%05d' $((i-328)))
  i4=$(printf '%05d' $((i-919)))
  i5=$(printf '%05d' $((i-1803)))
  ssc 640 360 frame$ii.png V5/$i5.png V2/$i2.png V3/$i3.png V4/$i4.png
done


for i in `seq 2518 2567` ; do
  ii=$(printf '%05d' $i)
  echo "Processing frame $ii"
  i2=$(printf '%05d' $((i-164)))
  i4=$(printf '%05d' $((i-919)))
  i5=$(printf '%05d' $((i-1803)))
  ssc 640 360 frame$ii.png V5/$i5.png V2/$i2.png V4/$i4.png
done


for i in `seq 2568 3117` ; do
  ii=$(printf '%05d' $i)
  echo "Processing frame $ii"
  i4=$(printf '%05d' $((i-919)))
  i5=$(printf '%05d' $((i-1803)))
  ssc 640 360 frame$ii.png V5/$i5.png V4/$i4.png
done


for i in `seq 3118 4045` ; do
  ii=$(printf '%05d' $i)
  echo "Processing frame $ii"
  i5=$(printf '%05d' $((i-1803)))
  cp V5/$i5.png output/frame$ii.png
done

