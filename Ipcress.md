# Ipcress!

![British quad size cinema poster for 1965 film "The Ipcress File"](https://upload.wikimedia.org/wikipedia/en/4/49/Ipcress_File_British_quad_poster.jpg)
*<span title="British quad size cinema poster for 1965 film The Ipcress File">Wiki</span> (<a href="//en.wikipedia.org/wiki/Wikipedia:Non-free_content_criteria#4" title="Wikipedia:Non-free content criteria">WP:NFCC#4</a>), <a href="//en.wikipedia.org/wiki/File:Ipcress_File_British_quad_poster.jpg" title="Fair use of copyrighted material in the context of The Ipcress File (film)">Fair use</a>, <a href="https://en.wikipedia.org/w/index.php?curid=40521190">Link</a>*

Let me confess a wild dream of mine -- I really would love to write a soundtrack! I would like my music to accompany action -- a story, a motion... I wonder how effective it would be. I wonder if and how my music would add that little "something" to a movie, or an anime. And there are great examples of most effective soundtracks!
The music composed by [Barry Grey](https://en.wikipedia.org/wiki/Barry_Gray) for [Gerry Anderson's "U.F.O."](https://en.wikipedia.org/wiki/UFO_(TV_series)), for instance; and of course "[The Ipcress File](https://en.wikipedia.org/wiki/The_Ipcress_File_(film))", by [John Barry](https://en.wikipedia.org/wiki/John_Barry_(composer)) (two barries -- how strange :smile: )

This one, for instance, I composed it as an imaginary soundtrack for an imaginary remake of "The Ipcress File", the wonderful movie with Michael Caine. BTW I simply LOVE John Barry's soundtrack!

So, here is my little "Ipcress":


<iframe width="560" height="315" src="https://www.youtube.com/embed/tfMAwSsdZsY?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[!["Ipcress" is (c) Eidon, Eidon@tutanota.com. All rights reserved.](http://img.youtube.com/vi/tfMAwSsdZsY/0.jpg)](http://www.youtube.com/watch?v=tfMAwSsdZsY "Ipcress!")


The video here has been created with [Feed-povray](https://gitlab.com/Eidonko/Permutation-feed-povray)
with seed 012344445. The command line parameters I used were:

```
read -r -d '' PIGMENT <<- EndOfPigment
pigment{gradient<0,1,0> scale 0.5 turbulence 1.15
color_map { [0.0 color Clear]
        [0.3 color Clear]
        [0.5 color rgb<0.35,0.40,1.0> ]
        [0.6 color Red ]
        } // end of color_map
} // end of pigment -------------
        EndOfPigment
feedpovray --input=012344445 --y=3 --z=6 --max-size=3 --output=012344445.pov \
               --prologue=prologue.000111222 \
           --pigment="$PIGMENT"
```

The resulting images were fed into [txt2srt](https://gitlab.com/Eidonko/txt2srt) with the following input file:

<pre>
IMAGE: 012344445
FORMAT: %04d
TYPE: png
FPS: 23.976

T: 00:00:00,000
S: Ipcress. By Eidon (Eidon@tutanota.com).
S: All rights reserved.
CMD: echo gmic -input $image -glow $param% -output output/$image
FROM: 20
TO: 80

T: 00:00:10,000
S: Inspired by the movie "The Ipcress File".
S: The track begins in 12/8, with tenor sax playing the
S: theme, and then double bass, timpani, and flute.
CMD: echo convert $image -region $((1280-iparam*2))x$((720-iparam))+${iparam}+${iparam} -fill '"rgba(173,255,47,1)"' -colorize 70% tmp-image.png \; gmic -input tmp-image.png -glow $((100 - iparam / 64))% -output output/$image
FROM: 640
TO: 1

T: 00:00:19,177
S: The theme is played again, but now a cello doubles
S: the sax. The timpani go a little crazy here XD
CMD: echo convert $image -region 1280x720+0+${iparam} -fill '"rgba(0,240,0,1)"' -colorize $((iparam / 8))% output/$image
FROM: 0
TO: 719

T: 00:00:38,000
S: Tempo here slows down from 150 to 132bpm, while a
S: second theme is exposed. Bass is a little funky here.
CMD: echo convert $image -region $((1280-iparam*2))x$((720-iparam))+${iparam}+${iparam} -fill '"rgba(173,255,47,1)"' -colorize 70% output/$image
FROM: 1
TO: 640


T: 00:00:52,800
S: The metre changes to 3/4 here, and the sax doubles.
CMD: echo convert $image -region 1280x720+0+${iparam} -fill '"rgba($((iparam / 8)),$((iparam / 8 - 240)),160,1)"' -colorize $((iparam / 8))% output/$image
FROM: 719
TO: 0

T: 00:01:07,200
S: Here Theme 1 reprises, though it is now made
S: "electric": sax is coupled by a distorted guitar
S: and contrabass is taken over by picked bass,
CMD: echo convert $image -region 1280x720+0+${iparam} -fill '"rgba(220,20,60,1)"' -colorize $((iparam / 8))% output/$image
FROM: 0
TO: 719

T: 00:01:22,000
S: which lead the track to its conclusion!
CMD: echo convert $image -region $((1280-iparam*2))x${iparam}+${iparam}+${iparam} -fill '"rgba(173,255,47,1)"' -colorize 70% tmp-image.png \; gmic -input tmp-image.png -glow $((100 - iparam / 64))% -output output/$image
FROM: 640
TO: 1

T: 00:01:32,160
</pre>

The above input file invokes [G'Mic](https://gmic.eu), which must be installed on your system.

**Ipcress** was composed by Eidon (Eidon@tutanota.com). Audio and video are (c) Eidon. All rights reserved.
