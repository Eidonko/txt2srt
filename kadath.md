# Kadath
Kadath is a music piece I composed. I also created a video for it, and I used `txt2srt` to apply some video effects. The input I used is as follows:

```
IMAGE: 012223333333
FORMAT: %04d
TYPE: png
FPS: 23.976

REM: $ gen.sh 
REM: convert-im6.q16: geometry does not contain image `0122233333334028.png' @ warning/transform.c/CropImage/717.
REM: 
T: 00:00:00,000
S: Kadath. By Eidon, Eidon@tutanota.com.
S: Musica e immagini (c) Eidon, All rights reserved.
CMD: echo cp $image output/$image

REM: A rectangular tile grows from (0,0) to (0,80), while the scene gets darkened up to %80
T: 00:00:10,700
S: Testo tratto da "La ricerca onirica dello sconosciuto
S: Kadath", di H. P. Lovecraft. Traduzione di Gianni Pilo.
CMD: echo convert $image -region 1280x720+0+${iparam} -fill '"rgba(0,0,0,1)"' -colorize ${param}% output/$image
FROM: 1
TO: 80

REM:  The tile retracts, while darkening stays at 80%
T: 00:00:22,000
S: L’intera zona sottostante non era altro che tenebre,
S: pervase di un sacro terrore, che si allungavano
S: da profondità infinite ad altezze infinite.
CMD: echo convert $image -region 1280x720+0+${iparam} -fill '"rgba(0,0,0,1)"' -colorize 80% output/$image
FROM: 80
TO: 1

REM:  Darkening gradually becomes reddening (255,0,0,1), with colorize 80%
T: 00:00:42,500
S: C’erano delle torri su quella vetta titanica, delle
S: orribili torri a cupola che si susseguivano l’una
S: accanto all’altra in innumerevoli ammassi nefasti,
CMD: echo convert $image -fill '"rgba(${iparam},0,0,1)"' -colorize 80% output/$image
FROM: 0
TO: 255

REM:  (255,0,0,1) -> (127,128,0,1), col 80%
T: 00:00:53,000
S: e bastioni e terrazze di uno splendore
S: stupefacente e minaccioso
CMD: echo convert $image -fill '"rgba(${iparam},$((255-iparam)),0,1)"' -colorize 80% output/$image
FROM: 255
TO: 127

REM:  (127,128,0,1) -> (255,0,128,1), col 80%
T: 00:01:04,000
S: che si stagliavano, neri e lontani,
S: contro l’arcata stellare che brillava
S: minacciosa sull’estremo orizzonte
CMD: echo convert $image -fill '"rgba(${iparam},$((255-iparam)),$((iparam-127)),1)"' -colorize 80% output/$image
FROM: 127
TO: 255

REM:  (255,0,128,1) -> (127, 127, 127, 1), col 80%
T: 00:01:14,000
S: e sorvegliavano con i loro orrori le immense distese
S: sconfinate di un gelido mondo polare che non è dell’uomo.
CMD: echo convert $image -fill '"rgba($((255 - iparam)),$iparam,127,1)"' -colorize 80% output/$image
FROM: 1
TO: 127

REM: (127, 127, 127, 1) -> (0, 0, 0, 1), col 80%
T: 00:01:25,100
S: Poi, improvvisamente, le nuvole si
S: diradarono, e le stelle splendettero spettrali dall’alto.
S: Più si allontanavano, più aumentava la velocità del volo,
CMD: echo convert $image -fill '"rgba($iparam,$iparam,$iparam,1)"' -colorize 80% output/$image
FROM: 127
TO: 0

REM: (0,0,0,1) -> (0,0,0,1), col 80% -> 30%
T: 00:01:35,000
S: finché la loro rapidità pazzesca sembrò superare
S: quella di un proiettile di fucile e raggiungere
S: quella di un pianeta in orbita.
CMD: echo convert $image -fill '"rgba(0,0,0,1)"' -colorize ${param}% output/$image
FROM: 80
TO: 30

REM: (0,0,0,1) -> (230,0,0,1), col 30%
T: 00:01:41,000
S: "Innalzati soltanto fino a quando
S: sentirai il canto lontano dell’etere più alto.
CMD: echo convert $image -fill '"rgba($iparam,0,0,1)"' -colorize 30% output/$image
FROM: 1
TO: 230

CMD: echo convert $image -fill '"rgba($((230 - iparam)),$iparam,$((3 * iparam)),1)"' -colorize $((30 + iparam))% output/$image
T: 00:01:46,500
S: Sopra di esso si annida la follia;
REM: (230,0,0,1) -> (190, 40, 120) col 30% -> 70%
FROM: 1
TO: 40


REM: (190, 40, 120) -> (10, 40, 120) col 70%
T: 00:01:55,800
S: quindi, trattieni lo Shantak quando sentirai
S: la prima nota."
CMD: echo convert $image -fill '"rgba($((190 - iparam)),40,120,1)"' -colorize 70% output/$image
FROM: 1
TO: 180

REM: (10, 40, 120) -> (10, 0, 0) col 30%
T: 00:02:06,500
S: Era una canzone, ma non proveniva da una voce umana.
S: La cantavano la notte e le sfere celesti,
CMD: echo convert $image -fill '"rgba(10,$((40 - iparam)),$((120 - iparam * 3)),1)"' -colorize $((70 - iparam))% output/$image
FROM: 1
TO: 40


REM: echo cp $image output/$image
REM: (10, 0, 0) -> (250, 250, 250) col 30% -> 60%
T: 00:02:14,500
S: e risaliva al tempo in cui erano nati
S: lo spazio, Nyarlathotep e gli Altri Dei.
CMD: echo convert $image -fill '"rgba($iparam,$((10 + iparam)),$((10 + iparam)),1)"' -colorize $((30 + iparam / 8))% output/$image
FROM: 1
TO: 240

REM: (250, 250, 250) col 60% -> 80%
REM: echo convert $image -region 1280x720+0+${iparam} -fill blue -colorize 80% output/$image
T: 00:02:24,000
S: Ci furono di nuovo un firmamento e il vento;
S: Ci furono gli dèi, le entità e i desideri;
S: la bellezza e il male;
CMD: echo convert $image -region 1280x720+0+$((250 - iparam * 8)) -fill '"rgba(250, 250, 250,1)"' -colorize 60% output/$image
FROM: 1
TO: 90

T: 00:02:38,500
S: e l’urlo lancinante della notte
CMD: echo cp $image output/$image

T: 00:02:43,900
S: che era stata derubata della sua preda.
CMD: echo cp $image output/$image

T: 00:02:48,000
S: Sognando gli dèi dello Sconosciuto Kadath
S: che tanto cercava
CMD: echo convert $image -region 1280x720+0+$((iparam * 8)) -fill '"rgba(250, 250, 250,1)"' -colorize 60% output/$image
FROM: 1
TO: 90

REM: 250 250 250 -> 50 250 50
T: 00:02:55,000
S: Carter era finalmente arrivato
S: sul Monte Sconosciuto, nel Gelido Deserto
CMD: echo convert $image -fill '"rgba($((250 - iparam)), 250, $((250 - iparam)),1)"' -colorize 60% output/$image
FROM: 1
TO: 200

T: 00:02:59,500
S: dove lo Sconosciuto Kadath, coperto dalle nuvole
S: ed incoronato di stelle ignote, custodisce in segreto,
CMD: echo cp $image output/$image

T: 00:03:10,500
S: avvolgendolo nella notte,
S: il Castello d’Onice dei Grandi Antichi
S: che gli stessi dèi negavano ai suoi sogni.
CMD: echo convert $image -fill '"rgba(50, $((250 - iparam)), 50, 1)"' -colorize 60% output/$image
FROM: 1
TO: 200


T: 00:03:20,000
S: Gli dèi del Sogno che meditano nascosti
S: sulle nuvole sullo sconosciuto monte Kadath,
CMD: echo cp $image output/$image

T: 00:03:29,500
S: nel Gelido Deserto in cui nessun uomo
S: osa avventurarsi.
CMD: echo convert $image -fill '"rgba($((50 + iparam)), $((50 - iparam)), $((50 - iparam)), 1)"' -colorize 60% output/$image
FROM: 1
TO: 30

T: 00:03:43,000
S: Testo: frammenti tratti da 
S: "The Dream-Quest of Unknown Kadath",
S: di Howard Phillips Lovecraft.
CMD: echo convert $image -fill '"rgba($((50 + iparam)), 20, 20, 1)"' -colorize 70% output/$image
FROM: 1
TO: 120

T: 00:03:48,410
```

The output is available here:
![Kadath. (c) Eidon, Eidon at tutanota.com. All rights reserved.](Kadath-9.mp4)


Note that video and music are (c) Eidon, Eidon at tutanota.com. All rights reserved.