Why do I compose music? And why do I experiment with generative compositions? I think I know now.

It's for the thrill.

The thrill that I get when certain magic combinations and ideas "work" beyond what I could ever have anticipated.
That is the case for this work. 

Let me explain a little.


I started to work on it two years ago. My game simulator provided me with three voices. 

I did something new. I only focused on one of them -- the one corresponding to the cards the players were accumulating "on the table" as a result of their moves. I had the idea that that voice was a theme. A theme for a fugue.

I started playing with it. For some reason, I decided that I was not ready yet. That piece required some time -- it had to "leaven up."

Yesterday evening I finally focused on it once more. I listened to it. I understood what it wanted me to do. I applied the voice in new ways, as I was instructed to do. From that voice.

And from the sum of its parts -- actually being the very same, repeated part -- this "Fugestalt"  (portmanteau of "Fuge" and "Gestalt") finally emerged.

And I finally realized why I compose and why I experiment.

Durata traccia 102.929s = 2468F

Kalimba, Kalimba II + Celesta, Tenor Sax, Oud + Kanuni + Sirian Bendir percussions, Piano.
                6.854s                        13.710s      38.341s                                                         75.207s  Totale 168.691
		6.854*23.976
		= 164.331504F
                                                   (13.710-6.854)*23.976
                                                   = 164.379456F
                                                                     (38.341-13.710)*23.976
                                                                     = 590.552856F
                                                                                                                                       (75.207-38.341)*23.976
                                                                                                                                       = 883.899216F
	 dopo 164 + 164 +         591 +                 884                        e fino a 4044

0       164             328            919                    1803       2468       2796    3213          4044

v1......v1+v2........v1+v2+v3...v1+v2+v3+v4.....v1....v5    v2...v5    v3..v5   v4,v5

+------------------------------------------------------------------------------------+                                                          2468
          +-----------------------------------------------------------------------------------------+                                           2632-164=2468
                            +---------------------------------------------------------------------------------+                                 2796-591
d=2385
0+d
164+d
328+d
919+d
1803+d
 0,2385
 164,2549
 328,2713
 919,3304
 1803,4188


1: 00994F
2: 01011F
3: 01178F
4: 00892F

5: 00221F
6: 00179F
7: 00215F
8: 02471F
9: 01734F

1280x720
720
https://eidon.bandcamp.com/track/fugestalt


------

I would like to share with you a piece called "Fugestalt", portmanteau of Fuge and Gestalt.

https://eidon.bandcamp.com/track/fugestalt

This track puzzles me seriously, 'cause it's algorithmic, and yet it seems not -- at least, to me.

A voice, created by the algorithm, is presented several times, overlapping it at different times and with different instruments.

I think something seems to happen, and my meager ability to understand fails me miserably.

May I ask for your opinion, and advise?

Thank you very much,
Eidon. 


---------


https://eidon.bandcamp.com/track/fugestalt
This track puzzles me seriously, 'cause it's algorithmic, and yet it seems not -- at least, to me.
A voice, created by the algorithm, is presented several times, overlapping it at different times and with different instruments.
I think something seems to happen, and my meager ability to understand fails me miserably.
It's a magical xenochrony
And I really cannot understand. What did I do??


=========

50 86.5
158.5 196
217 257
265 302
322 329.4
401.7 408
467.3 474.6
892.7 975  
1348 1407

2019 Mount Robson Marathon
https://archive.org/details/vctv7bc-2019_Mount_Robson_Marathon

by
    VCTV7


Publication date
    2019-09-30 

Topics
    Valemount, British Columbia, Valemount Community TV, VCTV7, Public Access TV, Community Media, PEG, Youtube, Mount Robson Marathon, Mt. Robson Marathon, VCTV, Valemount Community TV, Valemount, Mount Robson, 2019

Language
    English

VCTV (Valemount Community TV) coverage of the 2019 Mount Robson Marathon held on September 7, 2019 at Mount Robson, British Columbia. Camera: Michael Peters. Editor: Martin Gislimberti. Copyright VCTV 2019,

Addeddate
    2019-11-25 03:21:27

Duration
    1425

Identifier
    vctv7bc-2019_Mount_Robson_Marathon

Run time
    00:23:45

Scanner
    Internet Archive Python library 1.8.5

Year
    2019 

Youtube-height
    1080

Youtube-id
    yqp8tBykG6A

Youtube-n-entries
    967

Youtube-playlist
    Uploads from ValemountCommunityTV

Youtube-playlist-index
    956

Youtube-uploader
    ValemountCommunityTV 

--------


Why do I compose music? And why do I experiment with generative compositions? I think I know now.It's for the thrill.The thrill that I get when certain magic combinations and ideas "work" beyond what I could ever have anticipated.
That is what happened with this work. Let me explain a little.I started to work on it two years ago. My game simulator provided me with three voices. I did something new. I only focused on one of them -- the one corresponding to the cards the players were accumulating "on the table" as a result of their moves. I had the idea that that voice was a theme. A theme for a fugue.I started playing with it. For some reason, I decided that I was not ready yet. That piece required some time -- it had to "leaven up."A few days ago I finally decided to focus on this idea once more. I listened to it. I understood what it wanted me to do. I applied the voice in new ways, as I was instructed to do. From that voice.And from the sum of its parts -- actually being the very same, repeated part -- this "Fugestalt"  (portmanteau of "Fuge" and "Gestalt") finally emerged.And I finally realized why I compose and why I experiment.----------

The video took me a lot of time to complete. It uses chunks that I extracted from the video "2019 Mount Robson Marathon"(https://archive.org/details/vctv7bc-2019_Mount_Robson_Marathon). I wrote a software that combines the chunks following the voices of this fugue. I hope you'll like it.

----------
The song can be downloaded in lossless format from my #bandcamp, at https://eidon.bandcamp.com/track/fugestalt.

Please follow me on bandcamp, via https://eidon.bandcamp.com/follow_me, and on youtube
https://youtu.be/R5aKbLPNsbs
facebook, at https://www.facebook.com/EidonVeda.

Best,
Eidon

----

I now completed a video for this track. It took me a lot of time to complete... It uses chunks that I extracted from another video ("2019 Mount Robson Marathon", vctv7bc-2019_Mount_Robson_Marathon). I wrote a software that combines the chunks following the voices of this piece. I hope you'll like it!
https://fb.watch/e-EblFcCR8/