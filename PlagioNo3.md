# Plagio Bachiano no. 3

This is a more complex example of using `txt2srt`. Let me first show you the input to `txt2srt`:

```
IMAGE: 011111234
FORMAT: %04d
TYPE: png
FPS: 15

T: 00:00:00,000
S: Plagio Bachiano no. 3. By Eidon, Eidon@tutanota.com.
S: Music and pictures (c) Eidon, All rights reserved.
CMD: echo cp $image output/$image

REM: a green cube expands and fills the screen
T: 00:00:12,200
S: The theme is exposed by the first guitar.
CMD: echo convert $image -region $((1280-iparam*2))x$((720-iparam))+${iparam}+${iparam} -fill '"rgba(173,255,47,1)"' -colorize 70% output/$image
FROM: 640
TO: 1

T: 00:00:24,600
S: With the theme, a variation is proposed by the second guitar.
REM: CMD: echo convert $image -region 1280x720+0+${iparam} -fill '"rgba(0,0,0,1)"' -colorize 80% output/$image
CMD: echo convert $image -region $((1280-iparam*2))x$((720-iparam))+${iparam}+${iparam} -fill '"rgba(173,255,47,1)"' -colorize 70% output/$image
FROM: 1
TO: 640

T: 00:00:49,200
S: Then the theme...
CMD: echo convert $image -fill '"rgba($((255-2*iparam)),255,${iparam},1)"' -colorize 40% output/$image
FROM: 0
TO: 40

T: 00:00:50,700
S: Then the theme
S: ...followed by a counterpoint on guitar...
CMD: echo convert $image -fill '"rgba($((255-2*iparam)),255,${iparam},1)"' -colorize 60% output/$image
FROM: 41
TO: 80

T: 00:00:52,300
S: Then the theme
S: ...followed by a counterpoint on guitar
S: ...and contrabass, begin dancing together
CMD: echo convert $image -fill '"rgba($iparam,255,${iparam},1)"' -colorize 80% output/$image
FROM: 160
TO: 0

T: 00:01:13,750
S: Here, the dance is interrupted by the theme,
S: who silences all other voices
CMD: echo convert $image -region $((1280-iparam*2))x${iparam}+${iparam}+${iparam} -fill '"rgba(173,255,47,1)"' -colorize 70% output/$image
FROM: 1
TO: 640

T: 00:01:32,200
S: ...and slows down to...
CMD: echo convert $image -fill '"rgba($((173-iparam*3)),$((255-iparam*5)),$((47-iparam)),1)"' -colorize 70% output/$image
FROM: 1
TO: 47

T: 00:01:39,000
S: ...and slows down to
S: ...a new beginning! All the guitar voices
S: iare now interpreted by the piano.
CMD: echo convert $image -region 1280x720+0+${iparam} -fill '"rgba(240,240,240,1)"' -colorize $((iparam / 8))% output/$image
FROM: 719
TO: 0

T: 00:02:03,550
S: Again, theme and counterpointed voice are exposed.
S: The psychedelia then leads to the stretto... 
CMD: echo convert $image -region 1280x720+0+${iparam} -fill '"rgba(0,240,0,1)"' -colorize $((iparam / 8))% output/$image
FROM: 0
TO: 719

CMD: echo convert $image -fill '"rgba($((230 - iparam)),$iparam,$((3 * iparam)),1)"' -colorize $((30 + iparam))% output/$image
T: 00:02:28,300
S: The psychedelia then leads to the stretto
S: ...played by piano and contrabass
CMD: echo convert $image -region $((1280-iparam*2))x${iparam}+${iparam}+${iparam} -fill '"rgba(173,255,47,1)"' -colorize 70% output/$image
FROM: 640
TO: 1

T: 00:02:52,900
S: again, silenced by the theme...
CMD: echo convert $image -fill '"rgba($((235-2*iparam)),235,${iparam},1)"' -colorize 80% output/$image
FROM: 0
TO: 40

T: 00:03:11,500
S: ...which initiates a rallentando and,
S: ultimately, concludes.
CMD: echo convert $image -region $((1280-iparam*2))x$((720-iparam))+${iparam}+${iparam} -fill '"rgba(220,20,60,1)"' -colorize 70% output/$image
FROM: 1
TO: 640

T: 00:03:22,410
```

The above has been applied to the following 15fps frames:
![Input test video](Plagio_Bachiano_no-10.mp4)

The result is as follows:
![Output video](pb3-10.mp4)

Higher quality versions are available on youtube:

- [![](http://img.youtube.com/vi/pf262Anu3Ec/0.jpg)](http://www.youtube.com/watch?v=pf262Anu3Ec "Input image")
- [![](http://img.youtube.com/vi/M2c19rWI9FE/0.jpg)](http://www.youtube.com/watch?v=M2c19rWI9FE "Output image")


<!-- blank line -->

<figure class="video_container">
  <iframe src="https://drive.google.com/file/d/1U5aYoSGZICh9sFJdm8JTQDTa6jt_cPcU/preview" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- blank line -->


<!-- blank line -->

<figure class="video_container">
  <iframe src="https://drive.google.com/file/d/1WAttEcg8sFSFUUHUViNC_AsOftbbtip3/preview" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- blank line -->


<iframe width="560" height="315" src="https://www.youtube.com/embed/pf262Anu3Ec" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/M2c19rWI9FE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<figure class="video_container">
  <iframe src="https://drive.google.com/file/d/1U5aYoSGZICh9sFJdm8JTQDTa6jt_cPcU/preview" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<figure class="video_container">
  <iframe src="https://drive.google.com/file/d/1WAttEcg8sFSFUUHUViNC_AsOftbbtip3/preview" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Music and videos are (c) Eidon, Eidon@tutanota.com. All rights reserved.