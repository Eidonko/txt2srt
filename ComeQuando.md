# Come Quando Fuori Piove

![Come Quando Fuori Piove, by and (c) Eidon (Eidon at tutanota.com). All rights reserved.](cqfp.mp4)

[![Youtube video of 'Come Quando Fuori Piove'](http://img.youtube.com/vi/89cbz5DUdLA/0.jpg)](http://www.youtube.com/watch?v=89cbz5DUdLA "Come Quando Fuori Piove, by and (c) Eidon (Eidon at tutanota.com). All rights reserved.")

<iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=846724782/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="https://eidon.bandcamp.com/album/ii">II by Eidon</a></iframe>


https://eidon.bandcamp.com/album/ii

>>>
December 21, 2020: rain solstice --
no Jupiter + Saturn for me today.

Hence, *Musica*!
>>>

This track is called "Come Quando Fuori Piove" -- an [Italian mnemonic](https://www.treccani.it/vocabolario/come-quando-fuori-piove/) that one may translate as "like when it's raining outside", which is used to remember the sequence "**C**uori-**Qua**dri-**F**i**ori**-**Pi**cche" (hearts, diamonds, clubs, spades). The order in that sequence is important in various Italian card games (the Italian versions of Gin Rummy, Poker, and others) [apparently](https://www.dossier.net/games/azzardo/regole/poker.htm).

The lyrics are just permutations of the mnemonic:

<pre>
Fuori  piove  come   quando
Come   quando fuori  piove
Piove  come   quando fuori
Quando fuori  piove  come

Come   piove  fuori  quando
Quando come   piove  fuori
Fuori  quando come   piove
Piove  fuori  quando come
</pre>

It would be wonderful if they would constitute a magic square, as  [Sator Arepo](https://noblogo.org/i-miei-piccoli-grundgestalt/arepo) -- alas that's not the case :smile:

In what follows I focus on the video of the song, which I created with `txt2srt` and ImageMagick.


[[_TOC_]]


## Generating the Frames with PovRay and FeedPovRay
The frames of the video were created with [Feedpovray]()
as follows:

```
read -r -d '' PIGMENT <<- EndOfPigment
pigment {
        marble
        turbulence 0.5
        lambda 1.5
        omega 0.8
        octaves 5
        frequency 3
        color_map {
                [0.00 color Red]
                [0.33 color Blue]
                [0.66 color Yellow]
                [1.00 color Red]
        }
        rotate 45*z
}
        EndOfPigment
feedpovray --input=012233445 --y=3 --z=6 --max-size=3 --output=012233445.pov \
           --prologue=prologue.2.012222222 \
           --pigment="$PIGMENT"
```

The output file `012233445.pov` was edited by changing the camera as follows:
```
  location <160 + clock*8290, 160 + clock*8290, 160 + clock*8290>
  look_at   <-25,25,0>
```

This is the .ini file used to generate the frames:


```
Antialias=Off
Antialias_Threshold=0.1
Antialias_Depth=2

Input_File_Name="012233445.pov"

Initial_Frame=1
Final_Frame=2653
Initial_Clock=1
Final_Clock=0

Cyclic_Animation=on
Pause_when_Done=off

Width=1280
Height=720

```

## Adding special effects with `txt2srt`

This time I wanted to use the `dissolve` filter of ImageMagick to "merge" the PovRay frames
with some pictures with themes related to the rain.

I located three public domain pictures from `publicdomainpictures.net`:

- [Rain Drops And Shrubs](https://www.publicdomainpictures.net/en/view-image.php?image=150620&picture=rain-drops-and-shrubs)
  by axelle b. License: CC0 Public Domain.
  axelle b has released this “Rain Drops And Shrubs” image under Public Domain license. It means that you can use and modify it for your personal and commercial projects. If you intend to use an image you find here for commercial use, please be aware that some photos do require a model or a property release. Pictures featuring products should be used with care.
- [Rain On The Window](https://www.publicdomainpictures.net/en/view-image.php?image=150621&picture=rain-on-the-window)
  by axelle b. License: CC0 Public Domain.
  axelle b has released this “Rain On The Window” image under Public Domain license. It means that you can use and modify it for your personal and commercial projects. If you intend to use an image you find here for commercial use, please be aware that some photos do require a model or a property release. Pictures featuring products should be used with care.
- [Threat Of Rain](https://www.publicdomainpictures.net/en/view-image.php?image=23640&picture=threat-of-rain)
  by Bobbi Jones Jones. License: CC0 Public Domain.
  Bobbi Jones Jones has released this “Threat Of Rain” image under Public Domain license. It means that you can use and modify it for your personal and commercial projects. If you intend to use an image you find here for commercial use, please be aware that some photos do require a model or a property release. Pictures featuring products should be used with care.

![](https://www.publicdomainpictures.net/pictures/30000/velka/threat-of-rain.jpg)
*Here's “Threat Of Rain”, by Bobbi Jones Jones. CC0 Public Domain.*

Then, I defined the `txt2srt` command sequence, which I wrote in a file called `cqfp.stxt`.

```
IMAGE: 012233445
FORMAT: %04d
TYPE: png
FPS: 23.976

T: 00:00:00,000
S: 21 dicembre 2020: solstizio di pioggia,
S: niente Giove con Saturno oggi per me.
S: E allora, Musica!
CMD: echo gmic -input $image -glow $param% -output output/$image
FROM: 20
TO: 80

T: 00:00:08,000
S: Come Quando Fuori Piove. Di Eidon (Eidon@tutanota.com).
S: (c) Eidon. All rights reserved.
REM: echo convert $image -region $((1280-iparam*2))x$((720-iparam))+${iparam}+${iparam} -fill '"rgba(173,255,47,1)"' -colorize 70% tmp-image.png \; gmic -input tmp-image.png -glow $((100 - iparam / 64))% -output output/$image
REM: FROM: 640
REM: TO: 1
CMD: echo convert $image gouttes-de-pluie-et-arbustes-720.png -compose dissolve -define compose:args=${iparam} -composite output/$image
FROM: 0
TO: 200

T: 00:00:16,000
S: Il brano, iniziato con il ruolo centrale affidato
S: al pianoforte, lascia ora spazio al sax tenore
CMD: echo convert gouttes-de-pluie-et-arbustes-720.png $image -compose dissolve -define compose:args=${iparam} -composite output/$image
FROM: 0
TO: 200

T: 00:00:32,000
S: E' sempre al sax che è affidato il secondo
S: tema ed il suo sviluppo
CMD: echo convert $image threat-of-rain-720.png -compose dissolve -define compose:args=${iparam} -composite output/$image
FROM: 0
TO: 130

T: 00:01:00,000
S: Fuori  piove  come   quando
S: Come   quando fuori  piove
S: Piove  come   quando fuori
S: Quando fuori  piove  come
CMD: echo convert threat-of-rain-720.png $image -compose dissolve -define compose:args=${iparam} -composite output/$image
FROM: 0
TO: 200


T: 00:01:16,000
S: Dopo le corde pizzicate, ancora il sax
CMD: echo convert $image pluie-sur-la-fenetre-720.png -compose dissolve -define compose:args=${iparam} -composite output/$image
FROM: 0
TO: 130

T: 00:01:32,000
S: Torna quindi il canto, ed il brano si chiude:
S: Come   piove  fuori  quando / Quando come   piove  fuori
S: Fuori  quando come   piove / Piove  fuori  quando come!
CMD: echo convert pluie-sur-la-fenetre-720.png $image -compose dissolve -define compose:args=${iparam} -composite output/$image
FROM: 0
TO: 160

T: 00:01:50,643
```

Please note that here I used 1280x720 versions of the three above-mentioned
public-domain pictures.

Then I executed the following commands:

```
txt2srt < cqfp.stxt      # This creates `genscript.sh` and `subtitles.srt`
./genscript.sh > gen.sh  # This creates `gen.sh`
bash gen.sh              # This processes all frames and places the output frames in `output`
cd output
./ffmpeg.sh              # This creates the output video with `ffmpeg`.
```


## Copyright notices
Audio and video have been conceived and created by Eidon (eidon at tutanota.com). Both media are (c) Eidon (eidon at tutanota.com). All rights reserved.


