1) [0, 1:39.580]
2) [6.854, 1:47.052]
3) [13.710, 1:45]
4) [38.341, 2:10.984]
5) [1:15.207, 2:46.823] [2:48.691]

0  6.854 13.710 38.341 1:15.207 1:39.580 1:45 1:47.052 2:10 2:46.823 2:48.691

v1 v1      v1        v1        v1
     v2      v2        v2        v2           v2           v2
               v3        v3        v3           v3
                           v4        v4           v4           v4     v4
                                       v5           v5           v5     v5          v5    fade-------------------->

0  164    328      919      1803        2388      2517 2567      3117 4000          4045


v1 must consist of 2388 frames
dir1: 993. we need other 2388-993=1395 frames. Taken from the first in dir8.

v2 must consist of 2567-164=2403 frames
dir2: 1011. We need 2403-1011=1392 frames. Taken from dir8[1079..2470]

v3 must consist of 2517-328=2189 frames
dir3: 1178. We need 2189-1178=1011 frames. Taken from: dir5[1..221]; dir6[1..179]; dir7[1..215]; not enough!
dir3: 1794. We need 2189-1794=395 frames. Taken from dir9[1340..1734]
(file 1179 was missing. corrected)

v4 must consist of 3117-919=2198 frames
dir4: 892. We need 2198-892=1306 frames. Taken from dir8[1..1306]

v5 must consist of 4045-1803=2242 frames
dir2:1011. 2242-1011=1231 frames. From dir9[1..1231]


----

Now we have all frames of all videos. What we need is "orchestrate" the output video

1280x720

1..164:        V1
165-328:     V1, with V2, scaled 640x360 and merged at 0,0      i.e. sc.v2[640x360+0+0]
329-919:     V1, with sc.v2[640x360+0+0] & sc.v3[640x360+640+0]
920-1803:   V1, with sc.v2[640x360+0+0] & sc.v3[640x360+640+0] & sc.v4[640x360+0+360]
1803-2388: V5, with sc.v2[640x360+0+0] & sc.v3[640x360+320+0] & sc.v4[640x360+0+360] & sc.v1[640x360,+640+360]
2389-2517: V5, with sc.v2[640x360+0+0] & sc.v3[640x360+320+0] & sc.v4[640x360+0+360]
2518-2567: V5, with sc.v2[640x360+0+0] &                                       & sc.v4[640x360+0+360]
2568-3117: V5, with                                                                                sc.v4[640x360+0+360]
3118-4045: V5.

./ssc 640 360 poppo.png 00001.png 00002.png 00003.png 00004.png 00005.png

for i in seq 1 164:
  ii=printf i
  copia V1[i] in out/frame$ii.png

for i in seq 165 328:
  ii=printf i
  ssc 640 360 out/frame$ii.png V1[i] V2[i-164]

for i in seq 329 919:
  ii=printf i
  ssc 640 360 out/frame$ii.png V1[i] V2[i-164] V3[i-328]

for i in seq 920 1803:
  ii=printf i
  ssc 640 360 out/frame$ii.png V1[i] V2[i-164] V3[i-328] V4[i-919]

for i in seq 1804 2388:
  ii=printf i
  ssc 640 360 out/frame$ii.png V5[i-2387] V2[i-164] V3[i-328] V4[i-919] V1[i]

for i in seq 2389 2517:
  ii=printf i
  ssc 640 360 out/frame$ii.png V5[i-2387] V2[i-164] V3[i-328] V4[i-919]

for i in seq 2518 2567:
  ii=printf i
  ssc 640 360 out/frame$ii.png V5[i-2387] V2[i-164] V4[i-919]

for i in seq 2568 3117:
  ii=printf i
  ssc 640 360 out/frame$ii.png V5[i-2387] V4[i-919]

for i in seq 3118 4045:
  copia V5[i-2387] in out/frame$ii.png
