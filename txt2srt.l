%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

/*
T: tempo1
S: frase1
T: tempo2
S: frase2a
S: frase2b
T: tempo3
T: tempo4
S: frase4
T: tempo5
E:


diventa


1
tempo1 --> tempo2
frase1

2
tempo2 --> tempo3
frase2a
frase2b

3
tempo4 --> tempo5
frase4
*/

int n, i;
FILE *f, *sub, *ff;
int time2frame(char *t);

char  time[256];
char *sentences[20];
int   ssp = 0;
bool  first = true;
char *command;
char *image;
char *format;
char *type;
char *from;
char *to;
float fps = 23.976;
int to_from, a_da;
float step;

int total;

bool verbose = false;
%}

%%

^[Ff][Pp][Ss]:[ ].*	{
			fprintf(f, "\n\nfps=%s\n\n", yytext + 5);
			fps = (float) atof(yytext + 5);
			}

^[Rr][Ee][Mm]:[ ].*	{
			fprintf(f, "# %s\n", yytext + 5);
			}

^[Tt][Oo]:[ ].*		{
			if (to) free(to);
			to = strdup(yytext + 4);
			}

^[Ff][Rr][Oo][Mm]:[ ].*	{
			if (from) free(from);
			from = strdup(yytext + 6);
			}

^[Tt][Yy][Pp][Ee]:[ ].*	{
			if (type) free(type);
			type = strdup(yytext + 6);
			}

^[Ff][Oo][Rr][Mm][Aa][Tt]:[ ].*	{
			if (format) free(format);
			format = strdup(yytext + 8);
			}

^[Ii][Mm][Aa][Gg][Ee]:[ ].*	{
			if (image) free(image);
			image = strdup(yytext + 7);
		}
^[cC][Mm][Dd]:[ ].*	{
			if (command) free(command);
			command = strdup(yytext + 5);
		}

^[tT]:[ ].*	{
		if (first) {
			strcpy(time, yytext + 3);
			first = false;
			ssp = 0;
		} else {
			// not the first time, therefore time holds a valid time
			if (ssp==0) {
				strcpy(time, yytext + 3);
			} else {
				int da, a;
				int ifrom, ito;
				bool with_params = false;
				bool reverse = false;

				fprintf(sub, "%d\n", ++n);
				fprintf(sub, "%s --> %s\n", time, yytext + 3);
				if (command != NULL && strcmp(command, "NOP") != 0) {
					da = time2frame(time);
					a  = time2frame(yytext + 3);
					fprintf(f, "\n\nda=%d\n", da);
					fprintf(f, "a=%d\n", a);

					a_da = a - da;
					total += a_da;
					if (verbose) printf("da %d, a %d, a_da %d\n", da, a, a_da);

					if (verbose) printf("to %s, from %s\n", to, from);

					if (to != NULL && from != NULL) with_params = true;

					if (with_params) {

						ifrom = atoi(from);
						ito   = atoi(to);
						if (ito < ifrom) reverse = true;

						to_from = reverse? ifrom - ito : ito - ifrom;

						step = (float) to_from / a_da ;
						if (verbose) printf("step %f\n", step);
						fprintf(f, "step=\"%f\"\n", step);
						fprintf(f, "param=$da\n");
						fprintf(f, "from=%s\n", from);
						fprintf(f, "to=%s\n", to);
					}

					fprintf(f, "for i in `seq $da $a` ; do\n");

						// fprintf(f, "\tline=$((line++))\n");
						// fprintf(f, "\tpercent=$(bc -l <<< \"$line / $total\")\n");

						if (with_params) {
							if (reverse)
								fprintf(f, "\tparam=$(bc -l <<< \"$from - ($i - $da) * $step\")\n");
							else
								fprintf(f, "\tparam=$(bc -l <<< \"$from + ($i - $da) * $step\")\n");
							fprintf(f, "\tiparam=$(round $param)\n");
						}
						fprintf(f, "\tn=$(printf '%s' $i)\n", format);
						fprintf(f, "\timage=\"%s${n}.%s\"\n", image, type);

						fprintf(f, "\t%s\n", command);

					fprintf(f, "done\n");

					free(to);   to   = NULL;
					free(from); from = NULL;
				}
				strcpy(time, yytext + 3);
				for (i=0; i<ssp; i++) {
					fprintf(sub, "%s\n", sentences[i]);
					free (sentences[i]);
				}
				fputc('\n', sub);
				ssp = 0;
			}
		}
	}

^[sS]:[ ].*	{
		sentences[ssp++] = strdup(yytext + 3);
	}

^[eE]:.*	{
			fprintf(sub, "%d\n", ++n);
			fprintf(sub, "%s --> %s\n", time, yytext + 3);
			for (i=0; i<ssp; i++) {
				fprintf(sub, "%s\n", sentences[i]);
				free (sentences[i]);
			}
			fputc('\n', sub);
	}

(.|\n)	;

%%

int time2frame(char *tt) {
	 char *h, *m, *s, *ms, *p;
	 int  ih, im, is, ims;
	 int seconds;
	 char *t = strdup(tt);

	 p=strchr(t, ':');
	 if (p == NULL) return -1;
	 *p = '\0';
	 h = t;
	 m = p + 1;

	 p=strchr(p+1, ':');
	 if (p == NULL) return -1;
	 *p = '\0';
	 s = p + 1;

	 p=strchr(p+1, ',');
	 if (p == NULL) return -1;
	 *p = '\0';
	 ms = p + 1;

	ih = atoi(h); im = atoi(m); is = atoi(s); ims = atoi(ms);
	free(t);

	seconds = ih * 3600 + im * 60 + is;
	return (int) floor(seconds * fps + (ims / 1000.0) * fps + 0.5);
}

int main() {
	char buffer[60];
	strcpy(buffer, "#######################################################\n\n\n");
	f=fopen("genscript.sh", "w");
	sub=fopen("subtitles.srt", "w");
	fwrite (buffer , sizeof(char), strlen(buffer), f);
		fprintf(f, "\n# genscript.sh: created by txt2srt, by Eidon (eidon at tutanota.com)\n\n");
		fprintf(f, "line=1\n\n");
		fprintf(f, "compute() { echo $(awk \"BEGIN {print $1}\")\n}\n\n");
		fprintf(f, "icompute() { echo $(awk \"BEGIN {print int($1)}\")\n}\n\n");
		fprintf(f, "round() {\n\techo $(awk '{print ($0-int($0)<0.499)?int($0):int($0)+1}' <<< \"$1\")\n}\n\n");
		// fprintf(f, "echo \'icompute() { echo $(awk \"BEGIN {print int($1)}\")\n}\n\n'\n");



		while (yylex()) ;

		system("mkdir -p output");

		ff=fopen("output/ffmpeg.sh", "w");
		fprintf(ff, "#!/usr/bin/env bash\n\n");
		fprintf(ff, "\n# ffmpeg.sh: created by txt2srt, by Eidon (eidon at tutanota.com)\n\n");

fprintf(ff, "threads=\"-thread_queue_size 4096\"\n");
fprintf(ff, "threads=\"\"\n");
fprintf(ff, "duration=$(duration \"$1\")\n");
fprintf(ff, "title=${1%%.*}\n");
fprintf(ff, "minus=3\n");
fprintf(ff, "target=50 # 50MB\n");
fprintf(ff, "bitrate=$(python -c \"from math import ceil; print(ceil($target * 8192 / $duration))\")\n");
fprintf(ff, "# there's also 2' silence\n");
fprintf(ff, "reduceduration=$(python -c \"print($duration -${minus}.0 -2.0)\")\n");
fprintf(ff, "# no 2' of silence here\n");
fprintf(ff, "reduceduration=$(python -c \"print($duration -${minus}.0)\")\n");
fprintf(ff, "bitrate=$(python -c \"from math import ceil; print(ceil($bitrate-112))\")\n");
fprintf(ff, "echo $bitrate\n\n");
fprintf(ff, "fontsize=12\n\n");
fprintf(ff, "bottomleft=1\n");
fprintf(ff, "bottomcenter=2\n");
fprintf(ff, "bottomright=3\n");
fprintf(ff, "topleft=5\n");
fprintf(ff, "topcenter=6\n");
fprintf(ff, "topright=7\n");
fprintf(ff, "midleft=9\n");
fprintf(ff, "midcenter=10\n");
fprintf(ff, "midright=11\n\n");
fprintf(ff, "alignment=$bottomcenter\n");
fprintf(ff, "read -r -d '' f264 << EOM__\n");
fprintf(ff, "ffmpeg -framerate 23.976000 -pattern_type glob $threads -i 'myfigure-*.jpg' -i \"$1\"  -lavfi \"fade=in:st=0:d=4,fade=out:st=$reduceduration:d=${minus},subtitles=../subtitles.srt:force_style='Alignment=$alignment,OutlineColour=&H100000000,BorderStyle=3,Outline=1,Shadow=0,Fontsize=$fontsize'\" $threads -c:v libx264 -b:v ${bitrate}k -an -f null -r 23.976000 -pix_fmt yuv420p -pass 1 /dev/null\n");
fprintf(ff, "ffmpeg -y -framerate 23.976000 -pattern_type glob $threads -i 'myfigure-*.jpg' -i \"$1\" -lavfi \"fade=in:st=0:d=4,fade=out:st=$reduceduration:d=${minus},subtitles=../subtitles.srt:force_style='Alignment=$alignment,OutlineColour=&H100000000,BorderStyle=3,Outline=1,Shadow=0,Fontsize=$fontsize'\"  $threads -c:v libx264 -b:v ${bitrate}k -c:a aac -b:a 96k -r 23.976000 -pix_fmt yuv420p -pass 2  \"$title\"-\"$target\"MB.mp4\n");
fprintf(ff, "EOM__\n");
fprintf(ff, "read -r -d '' f265 << EOM__\n");
fprintf(ff, "ffmpeg -framerate 23.976000 -pattern_type glob $threads -i 'myfigure-*.jpg' -i \"$1\"  -lavfi \"fade=in:st=0:d=4,fade=out:st=$reduceduration:d=${minus},subtitles=../subtitles.srt:force_style='Alignment=$alignment,OutlineColour=&H100000000,BorderStyle=3,Outline=1,Shadow=0,Fontsize=$fontsize'\" $threads -c:v libx265 -b:v ${bitrate}k -x265-params pass=1 -an -f null -r 23.976000 -pix_fmt yuv420p /dev/null\n");
fprintf(ff, "ffmpeg -y -framerate 23.976000 -pattern_type glob $threads -i 'myfigure-*.jpg' -i \"$1\" -lavfi \"fade=in:st=0:d=4,fade=out:st=$reduceduration:d=${minus},subtitles=../subtitles.srt:force_style='Alignment=$alignment,OutlineColour=&H100000000,BorderStyle=3,Outline=1,Shadow=0,Fontsize=$fontsize'\"  $threads -c:v libx265 -b:v ${bitrate}k -x265-params pass=2 -c:a aac -b:a 96k -r 23.976000 -pix_fmt yuv420p \"$title\"-\"$target\"MB.mp4\n");
fprintf(ff, "EOM__\n");


fprintf(ff, "ffmpeg -y -framerate %f -pattern_type glob $threads -i '%s*.%s' -i \"$1\"  $threads -c:v libx264 ", fps, image, type);
fprintf(ff, "-filter:v \"fade=in:st=0:d=4,fade=out:st=$reduceduration:d=${minus},subtitles=../subtitles.srt\" ");
fprintf(ff, "-b:v ${bitrate}k -c:a aac -b:a 128k -r %f -pix_fmt yuv420p \"$title\".mp4\n", fps);

		fclose(ff);
	fclose(sub);
	fclose(f);

	// prepend total 
	f=fopen("genscript.sh", "r+");
		sprintf(buffer, "#!/usr/bin/env bash\n\ntotal=%d\nfps=%f\n\n", total, fps);
		fwrite (buffer , sizeof(char), strlen(buffer), f);
	fclose(f);

	if (image)   free(image);
	if (command) free(command);
	if (format)  free(format);
	if (type)    free(type);
	if (from)    free(from);
	if (to)      free(to);

	system("chmod +x genscript.sh");
	system("chmod +x output/ffmpeg.sh");

	f=fopen("Makefile", "w");
	if (f==NULL) return 1;

	fprintf(f, "INPUT=input.stxt\n");
	fprintf(f, "MUSIC=input.flac\n");
	fprintf(f, "\n");
	fprintf(f, "all:\toutput.mp4\n\n");
	fprintf(f, "output.mp4:\t$(INPUT)\n");
	fprintf(f, "\ttxt2srt < $(INPUT)\n");
	fprintf(f, "\t./genscript.sh > gen.sh\n");
	fprintf(f, "\tbash gen.sh\n");
	fprintf(f, "\tcd output\n");
	fprintf(f, "\t./ffmpeg.sh $(MUSIC)\n");

	fclose(f);

	return 0;
}
