Supponiamo di voler creare un video a partire da tre immagini 

Le immagini sono nei file 1.jpg, 2.jpg e 3.jpg e devono essere della stessa dimensione (se così non è, si possono adattare creando cornici, scalando le immagini eccetera)

Supponiamo di voler creare un video di 38 secondi, che mostri tre spezzoni da 1.jpg, 2.jpg e 3.jpg

Il primo passo è creare tre spezzoni video da 10 secondi:

```ffmpeg -r 23.976 -loop 1 -i 3.jpg  -c:v libx264 -tune stillimage -preset  ultrafast -ss 00:00:00 -t 00:00:10  -pix_fmt yuv420p  -shortest 3.mp4 -y

Supponiamo di voler creare un video che parta con 1.jpg per 7 secondi, poi faccia dissolvenza per 3 secondi
